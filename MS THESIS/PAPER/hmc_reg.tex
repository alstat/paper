\chapter{\normalsize STOCHASTIC GRADIENT HAMILTONIAN MONTE CARLO}\label{ch:sghmc}
{\baselineskip=2\baselineskip
\lettrine{C}{\,lassical or frequentist} approach to modelling assumes the parameters $\mathscr{P}$ to be nonrandom and unknown, and inference is done using \textit{maximum likelihood estimation} (MLE). In Bayesian statistics, these parameters are assumed to exhibit randomness that can be described by a probability distribution called the \textit{posterior} distribution. This distribution is proportional to the \textit{likelihood of the data} and the prior knowledge of the parameters governed by \textit{a priori} distribution. From (\cite{casellaberger}), this is a subjective distribution based on the experimenter's belief, and is formulated before the data are seen. A sample is then taken from a population indexed by $\mathscr{P}$ and the prior distribution is updated with this sample information. The updated prior is then the posterior distribution. This updating is done with the use of Bayes' Rule given below
\begin{equation}
\p(\mathscr{P}|\mathscr{D})=\frac{\p(\mathscr{D}|\mathscr{P})\p(\mathscr{P})}{\p(\mathscr{D})}.
\end{equation}
The only factor that is often difficult to derive, especially for interesting models, is the normalizing factor $\p(\mathscr{D})=\int\p(\mathscr{D},\mathscr{P})\D\mathscr{P}$. This probability is often intractable due to high dimensional integration. As a result, Bayesian statisticians resort into different approximation techniques for characterizing the \textit{a posteriori}, $\p(\mathscr{P}|\mathscr{D})$.
\section{Hamiltonian Dynamics}\label{sec:hamiltoniandyn}
One limitation of the Metropolis-Hasting algorithm is that, the samples drawn are based on the rejection criterion where the decision to accept exhibits a random walk behaviour. The problem with this nature of computation is that the distance traversed through the state space grows only as the square root of the number of steps (\cite{bishop}). Obviously, one might attempt to increase the step size to shorten the distance traversed, but this will lead to high rejection rate.
The \textit{Hamiltonian Monte Carlo} --- originally known as \textit{Hybrid Monte Carlo} in the paper by \cite{Duane1987}, addresses the issue in the Metropolis-Hasting by considering auxiliary variable for describing the physical system in drawing samples from the target distribution. To understand the process, a brief review in Physics is given for \textit{Hamiltonian dynamics}. \textit{Dynamic} in Physics deals with the study of the causes of motion, that is, the physical factors that can affect the object's motion in the \textit{system} --- the event or phenomenon being studied. In particular, Hamiltonian dynamics describe the system using \textit{location} parameter notated as $\mathbf{w}$ and \textit{momentum} parameter $\mathbf{p}$.
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\centering
\begin{tikzpicture}
    \coordinate (origo) at (0,1.3);
    \coordinate (pivot) at (1,5);

    % draw axes
    \fill[black] (origo) circle (0.05);
    %\draw[thick,gray,->] (origo) -- ++(-3,0) node[black,left] {\tiny $x$};
    %\draw[thick,gray,->] (origo) -- ++(0,-4) node (mary) [black,below] {$y$};

    % draw roof
    \fill[pattern = north east lines] ($ (origo) + (-1,0) $) rectangle ($ (origo) + (1,0.5) $);
    \draw[thick] ($ (origo) + (-1,0) $) -- ($ (origo) + (1,0) $);

    \draw[thick] (origo) -- ++(305:4.7) coordinate (bob);
    \draw[thick, gray1] (origo) -- ++(279:4.7) coordinate (bob1);
    \draw[thick, gray2] (origo) -- ++(276:4.7) coordinate (bob2);
    \draw[thick, gray3] (origo) -- ++(273:4.7) coordinate (bob3);
    \draw[thick] (origo) -- ++(270:4.7) coordinate (bob4);
    \draw[thick, gray1] (origo) -- ++(241:4.7) coordinate (bob5);
    \draw[thick, gray2] (origo) -- ++(238:4.7) coordinate (bob6);
    \draw[thick] (origo) -- ++(235:4.7) coordinate (bob7);
    \fill (bob) circle (0.17);
    \fill[gray1] (bob1) circle (0.2);
    \fill[gray2] (bob2) circle (0.2);
    \fill[gray3] (bob3) circle (0.2);
    \fill (bob4) circle (0.2);
    \fill[gray1] (bob5) circle (0.2);
    \fill[gray2] (bob6) circle (0.2);
    \fill (bob7) circle (0.2);

    \node at (-3, -3.) {\tiny max PE};
    \node at (-3, -3.25) {\tiny min KE};

    \node at (0, -4.) {\tiny min PE};
    \node at (0, -4.25) {\tiny max KE};

    \node at (3, -3.) {\tiny max PE};
    \node at (3, -3.25) {\tiny min KE};
  \end{tikzpicture}
  \caption{Energies in Physical Pendulum.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p34}
\caption{Energies in Phase Space.}
\label{fig:hmcenergy:ps}
\end{subfigure}
\end{minipage}
\caption[Conversion of Energies in Physical Pendulum and Phase Space]{\it Conversion of Energies in Physical Pendulum and Phase Space.}
\label{fig:hmcenergy}
\end{figure}
As an example, consider a ball attached to a frictionless pendulum swinging on a vertical plane (\textit{see} Figure \ref{fig:hmcenergy}). For each location of the ball given by $\mathbf{w}$, there is a corresponding \textit{potential energy} (PE), denoted by $\mathbb{U}(\mathbf{w})$. And for each momentum $\mathbf{p}$, there is an associated \textit{kinetic energy} (KE) $\mathbb{K}(\mathbf{p})$. So that at the extreme trajectory of the pendulum, the PE is maximum and KE is minimum; and at the equilibrium point, the KE is maximum and PE is minimum. The system is a function of time, hence the Hamiltonian dynamics evolve in a continuous \textit{space} called \textit{phase space} (\textit{see} Figure \ref{fig:hmcenergy:ps}). Another example which is being used in many literature (\textit{see} \cite{tchen}; and \cite{neal2011}), imagine a hockey puck sliding over a frictionless ice surface of varying height. The PE term is based on the height of the surface at the current puck position, $\mathbf{w}$, while the KE is based on the momentum of the puck, $\mathbf{p}$, and its mass, $\boldsymbol{\Sigma}$. If the surface is flat, the puck moves at a constant velocity. For positive slopes, the KE decreases as the PE increases until the KE is at its minimum. The puck then slides back down the hill increasing its KE and decreasing its PE. 

The total energy of the system is characterized by the Hamiltonian $\mathbb{H}(\mathbf{w},\mathbf{p})\triangleq\mathbb{U}(\mathbf{w})+\mathbb{K}(\mathbf{p})$, and therefore describes the conversion of the two energies as the object moves throughout a system in time. So that the following are the Hamiltonian equations:
\begin{align}\label{eq:hamiltonian}
\begin{split}
\frac{\D \mathbf{w}}{\D t}&=\frac{\partial \mathbb{H}(\mathbf{w}, \mathbf{p})}{\partial \mathbf{p}}=\frac{\D \mathbb{K}(\mathbf{p})}{\D \mathbf{p}}\\
\frac{\D \mathbf{p}}{\D t}&=-\frac{\partial \mathbb{H}(\mathbf{w},\mathbf{p})}{\partial \mathbf{w}}=-\frac{\D \mathbb{U}(\mathbf{w})}{\D \mathbf{w}}.
\end{split}
\end{align}
The proof of this equations can be found in several Physics books, for those interested refer to . For this thesis, the derivation proceeds by illustration. To start with, let 
\begin{equation}
\frac{\D \mathbf{w}}{\D t}=V_{\mathbf{w}}(\mathbf{w},\mathbf{p})\quad\text{and}\quad\frac{\D \mathbf{p}}{\D t}=V_{\mathbf{p}}(\mathbf{w}, \mathbf{p}).
\end{equation}
Then
\begin{equation}
V_{\mathbf{w}}(\mathbf{w},\mathbf{p})=\frac{\partial \mathbb{H}(\mathbf{w}, \mathbf{p})}{\partial \mathbf{p}}\quad\text{and}\quad V_{\mathbf{p}}(\mathbf{w}, \mathbf{p})=-\frac{\partial \mathbb{H}(\mathbf{w}, \mathbf{p})}{\partial \mathbf{w}}.
\end{equation}

\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[t]{.5\textwidth}
\includegraphics[width=\textwidth]{hmc1.pdf}
\caption{Field Lines of $\vec{V}(\mathbf{w}, \mathbf{p})$.}
\label{fig:hmc_fieldlinesa}
\end{subfigure}
\begin{subfigure}[t]{.5\textwidth}
\includegraphics[width=\textwidth]{hmc2.pdf}
\caption{$\frac{\D [\mathbf{w}(t)\;\mathbf{p}(t)]}{\D t}$ over $\vec{V}(\mathbf{w}, \mathbf{p})$.}
\label{fig:hmc_fieldlinesb}
\end{subfigure}
\end{minipage}
\caption[Field Lines of $\vec{V}(\mathbf{w}, \mathbf{p})$ and Gradient of $\frac{\D [\mathbf{w}(t)\;\mathbf{p}(t)]}{\D t}$]{\it Field Lines of $\vec{V}(\mathbf{w}, \mathbf{p})$ and Gradient of $\frac{\D [\mathbf{w}(t)\;\mathbf{p}(t)]}{\D t}$.}
\label{fig:hmc_fieldlines}
\end{figure}

Now consider the vector field $\vec{V}(\mathbf{w}, \mathbf{p})=\left[\begin{array}{c}V_{\mathbf{w}}\\V_{\mathbf{p}}\end{array}\right]$, the plot of this is given in Figure \ref{fig:hmc_fieldlinesa}. While the illustration in here is applied to two-dimensional case, it should be understood that the notation for $\mathbf{w}$ and $\mathbf{p}$ remains in ``bold'' to emphasize that this is not constrained to plane only but also for high dimensions. So that by tracing the dynamics over time for vector $[\mathbf{w}(t)\;\mathbf{p}(t)]^{\text{T}}$ (\textit{see} Figure \ref{fig:hmc_fieldlinesb}), it follows that $\nabla[\mathbf{w}(t)\;\mathbf{p}(t)]^{\text{T}}=\vec{V}(\mathbf{w}(t), \mathbf{p}(t))$. That is $\nabla[\mathbf{w}(t)\;\mathbf{p}(t)]^{\text{T}}$ is tangent to the field lines of $\vec{V}(\mathbf{w}(t), \mathbf{p}(t))$. Next is to relate this to the Hamiltonian equation. Note that the Hamiltonian energy function is a scalar function. So the levels of the contour in Figure \ref{fig:hmc1_fieldlines} corresponds to the values of $\mathbb{H}(\mathbf{w}, \mathbf{p})$. Now clearly the trajectories of the gradient of $\mathbb{H}$ is perpendicular to the contour lines. This is because the gradient vector will point outward to the levels of the contour. So that,
\begin{equation}\label{eq:hmc1}
\nabla\mathbb{H}(\mathbf{w},\mathbf{p})=\left[\begin{array}{c}
\displaystyle\frac{\partial\mathbb{H}(\mathbf{w},\mathbf{p})}{\partial\mathbf{w}}\\[.3cm]
\displaystyle\frac{\partial\mathbb{H}(\mathbf{w},\mathbf{p})}{\partial\mathbf{p}}
\end{array}\right].
\end{equation}
This equation is now getting close to the objective gradient given in Equation (\ref{eq:hamiltonian}). The final turn is to rotate the gradient of $\mathbb{H}$, $90^{\circ}$ clockwise. By doing this, the vector field of $\nabla\mathbb{H}(\mathbf{w},\mathbf{p})$ is now tangent to the contour of $\mathbb{H}$, \textit{see} Figure \ref{fig:hmc1_fieldlinesb}. This in effect is equivalent to simply rotating the axes $90^{\circ}$ clockwise, so that $\mathbf{p}$ becomes $\mathbf{w}$, and $\mathbf{w}$ becomes $-\mathbf{p}$, and their corresponding placeholder in the vector will also interchange. Thus Equation (\ref{eq:hmc1}) becomes
\begin{equation}\label{eq:hmc2}
\operatorname{rot}^{90^{\circ}}\nabla\mathbb{H}(\mathbf{w},\mathbf{p})=\left[\begin{array}{c}
\displaystyle\frac{\partial\mathbb{H}(\mathbf{w},\mathbf{p})}{\partial\mathbf{p}}\\[.3cm]
\displaystyle-\frac{\partial\mathbb{H}(\mathbf{w},\mathbf{p})}{\partial\mathbf{w}}
\end{array}\right].
\end{equation}
And therefore Equation (\ref{eq:hmc2}) is equivalent to Equation (\ref{eq:hamiltonian}).
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[t]{.5\textwidth}
\includegraphics[width=\textwidth]{hmc3.pdf}
\caption{Contour Lines of $\mathbb{H}(\mathbf{w}, \mathbf{p})$.}
\label{fig:hmc1_fieldlinesa}
\end{subfigure}
\begin{subfigure}[t]{.5\textwidth}
\includegraphics[width=\textwidth]{hmc4.pdf}
\caption{$\nabla[\mathbf{w}(t)\;\mathbf{p}(t)]$ over $\mathbb{H}(\mathbf{w}, \mathbf{p})$.}
\label{fig:hmc1_fieldlinesb}
\end{subfigure}
\end{minipage}
\caption[{Contour Lines of $\mathbb{H}(\mathbf{w}, \mathbf{p})$ and Gradient of $[\mathbf{w}(t)\;\mathbf{p}(t)]$}]{\it Contour Lines of $\mathbb{H}(\mathbf{w}, \mathbf{p})$ and Gradient of $[\mathbf{w}(t)\;\mathbf{p}(t)]$.}
\label{fig:hmc1_fieldlines}
\end{figure}

There are three properties that makes Hamiltonian dynamics good for sampling methods. The first one is the \textit{conservation} of the energy, that is, a change in time for both position and momentum won't affect the Hamiltonian total energy. This can be seen from the following equation
\begin{align}
\frac{\D\mathbb{H}}{\D t}&=\sum_i\left[\frac{\partial \mathbb{H}}{\partial \mathbf{w}}\frac{\D \mathbf{w}}{\D t}+\frac{\partial \mathbb{H}}{\partial \mathbf{p}}\frac{\D \mathbf{p}}{\D t}\right]\\
&=\sum_i\left[\frac{\partial \mathbb{H}}{\partial \mathbf{w}}\frac{\partial \mathbb{H}}{\partial \mathbf{p}}-\frac{\partial \mathbb{H}}{\partial \mathbf{p}}\frac{\partial \mathbb{H}}{\partial \mathbf{w}}\right]=0.
\end{align}
The second property follows from the \textit{Liouville's theorem}, which claims that the system \textit{preserves} the volume of the phase space. And the last property is \textit{reversibility}, all details can be found in \cite{Neal1996}, \cite{bishop} and \cite{neal2011}. The kinetic energy is often assumed to be standard Gaussian distributed, and thus
$$
\mathbb{K}(\mathbf{p}, \boldsymbol{\mu}\triangleq \mathbf{0}, \boldsymbol{\Sigma}\triangleq \mathbf{I})=\frac{(\mathbf{p}-\boldsymbol{\mu})^{\text{T}}\boldsymbol{\Sigma}^{-1}(\mathbf{p}-\boldsymbol{\mu})}{2}=\frac{\mathbf{p}^{\text{T}}\mathbf{p}}{2}.
$$
where $\boldsymbol{\mu}$ and $\boldsymbol{\Sigma}$ are the mean vector and the variance-covariance matrix, respectively. The $\boldsymbol{\Sigma}$ of the kinetic energy can be assigned to any positive definite matrix if additional information about the target density is available.
\begin{algorithm}[!p]
\caption{\it Hamiltonian MCMC}
\label{algo:mcmch}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize Leap Frog parameters: $\varepsilon$ and $\tau$; the parameters of target distribution: $\mathscr{P}$; and the set of samples $\mathscr{S}=\{\boldsymbol{\theta}_r:\boldsymbol{\theta}_r\sim\mathbb{U}(\boldsymbol{\theta}), \forall r\}$\vspace{.2cm}
\item Define the Potential Energy (Target Distribution), $\mathbb{U}(\mathbf{w})$; and the Kinetic Energy (Auxiliary Distribution), $\mathbb{K}(\mathbf{p})$;\vspace{.2cm}
\item Define the gradient of these energies: $\nabla_{\mathbf{w}} \mathbb{U}$ and $\nabla_{\mathbf{p}}\mathbb{K}$;\vspace{.2cm}
\item Set initial location $\mathbf{w}_{r = 0}(t = 0)$;\vspace{.2cm}
\For {$r\in\{0,\cdots,r_{\text{max}}\}$}\vspace{.2cm}
\State Draw initial momentum, $\mathbf{p}_{r}(t = 0)\sim\mathbb{K}(\mathbf{p})$;\vspace{.2cm}
\State Compute the Hamiltonian $\mathbb{H}(\mathbf{w}_{r}(0),\mathbf{p}_{r}(0))\triangleq\mathbb{U}(\mathbf{w}_{r}(0))+\mathbb{K}(\mathbf{p}_r(0))$;
\vspace{.2cm}
\State Simulate Hamiltonian dynamics using Leap Frog:\vspace{.2cm}
\For {$t\in\{0,\cdots,\tau\}$}\vspace{.2cm}
\begin{align}
\mathbf{p}_r(t + \varepsilon/2)&\triangleq\mathbf{p}_r(t)-(\varepsilon/2)\frac{\partial \mathbb{U}(\mathbf{w}_r(t))}{\partial \mathbf{w}_r(t)}\\
\mathbf{w}_r(t + \varepsilon)&\triangleq\mathbf{w}_r(t)+\varepsilon\frac{\partial \mathbb{K}(\mathbf{p}_r(t + 1))}{\partial \mathbf{p}_r(t + 1)},\\
\mathbf{p}_r(t + \varepsilon)&\triangleq\mathbf{p}_r(t+\varepsilon/2)-(\varepsilon/2)\frac{\partial \mathbb{U}(\mathbf{w}_r(t+\varepsilon))}{\partial \mathbf{w}_r(t+\varepsilon)}
\end{align}
\EndFor\vspace{.2cm}
%\State Compute the new Hamiltonian $\mathbb{H}'(\mathbf{w}_r(\tau + \varepsilon),\mathbf{p}_r(\tau + \varepsilon))$;\vspace{.2cm}
%\State Compute the changes in total energy: $\Delta\mathbb{H} = \mathbb{H}' - \mathbb{H}$;\vspace{.2cm}
\If {$\Delta\mathbb{H} < 0$}\vspace{.2cm}
\State $\boldsymbol{\theta}_r\leftarrow \mathbf{w}_r(\tau+\varepsilon)$\vspace{.2cm}
\Else\vspace{.2cm}
 \If {$a < \exp\left(\Delta\mathbb{H}\right), a\sim\text{Unif}(0, 1)$}\vspace{.2cm}
  \State $\boldsymbol{\theta}_r\leftarrow \mathbf{w}_r(\tau+\varepsilon)$\vspace{.2cm}
 \Else \vspace{.2cm}
  \State $\boldsymbol{\theta}_r\leftarrow \mathbf{w}_r(0)$\vspace{.2cm}
 \EndIf\vspace{.2cm}
\EndIf\vspace{.2cm}
\EndFor\\\vspace{.2cm}
\Return $\mathscr{S}$
\end{algorithmic}
\end{algorithm}
\subsection{Leap Frog Method} 
Since the phase space changes over time which is a continuous variable, then in order to simulate the Hamiltonian dynamics under numerical computations, the time has to be discretized. And there are several ways to do this, one such solution is to consider the \textit{leap frog method}, which works as follows:
\begin{align}
\mathbf{p}(t+\varepsilon/2)&=\mathbf{p}(t)-(\varepsilon/2)\frac{\partial \mathbb{U}(\mathbf{w}(t))}{\partial \mathbf{w}(t)}\\
\mathbf{w}(t+\varepsilon)&=\mathbf{w}(t)+\varepsilon\frac{\partial \mathbb{K}(\mathbf{p}(t))}{\partial \mathbf{p}(t)},\\
\mathbf{p}(t+\varepsilon)&=\mathbf{p}(t+\varepsilon/2)-(\varepsilon/2)\frac{\partial \mathbb{U}(\mathbf{w}(t+\varepsilon))}{\partial \mathbf{w}}
\end{align}
where $\varepsilon > 0$.
\subsection{Hamiltonian/Hybrid Monte Carlo}
So how is Hamiltonian dynamics linked to MCMC? Well, it turns out that the total energy is related to the probability distribution of the parameter of interest using the concept of \textit{canonical distribution} from the Statistical Mechanics. That is,
\begin{equation}
\p(\mathscr{P})=\frac{1}{Z}\exp\left[-E(\mathscr{P})\right],
\end{equation}
where $E$ is the total energy. Therefore, $E$ in this case, is $\mathbb{H}(\mathbf{w},\mathbf{p})$. And using the three properties of Hamiltonian dynamics mentioned above, the canonical distribution is \textit{invariant}. So that the equation becomes
\begin{align}
\p(\mathbf{w},\mathbf{p})&\propto\exp\left[-\mathbb{H}(\mathbf{w},\mathbf{p})\right]\\
&=\exp\left[-\mathbb{U}(\mathbf{w})-\mathbb{K}(\mathbf{p})\right]\\
&=\exp\left[-\mathbb{U}(\mathbf{w})\right]\exp\left[-\mathbb{K}(\mathbf{p})\right]\\
&\propto\p(\mathbf{w})\p(\mathbf{p}).
\end{align}
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p35}
\caption{Random Samples.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p36}
\caption{Kernel Density Estimate.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p37}
\caption{Autocorrelation of $\theta_1$.}
\label{fig:mcmchmc:w1corr}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p38}
\caption{Autocorrelation of $\theta_2$.}
\label{fig:mcmchmc:w2corr}
\end{subfigure}
\end{minipage}
\caption[MCMC Hamiltonian on Target Density]{\it MCMC Hamiltonian on Target Density.}
\label{fig:mcmchmc}
\end{figure}
Therefore the joint canonical distribution of the location parameter $\mathbf{w}$ and the momentum parameter $\mathbf{p}$ factors into the products of its marginal density, implying independence. In this case, the momentum parameter serves as the \textit{auxiliary variable} for the parameter of interest, $\mathbf{w}$. And because this parameter is used for simulating Hamiltonian dynamics in phase space, making it random plus its gradient will lead to the exploration of high probability region, and thus it is also considered as the first proposal distribution with certainty of acceptance on the proposed samples. However due to the descritization of the phase space, the true samples proposed from the joint distribution of the location and momentum parameters are accepted using some adjustment. This adjustment is the introduction of Metropolis-Hasting criterion as the decision rule for accepting the sample. Needless to say, the normalizing term of the canonical distribution will be cancelled out --- this is evident in line 11 of Algorithm \ref{algo:mcmch}. 

To summarize, the parameter of interest $\mathbf{w}$ has the following target distribution,
\begin{equation}
\mathbb{U}(\mathbf{w})=-\log[\p(\mathbf{w})\mathcal{L}(\mathbf{w}|\mathscr{D})].
\end{equation}
It should be understood that $\mathbf{w}$ in this case represents the weights of the model. So in general, if $\mathscr{P}$ is the set of parameters or weights, and $\mathbf{y}\in\mathscr{D}$ then
\begin{align}
\mathbb{U}(\mathscr{P})&=-\log[\p(\mathscr{P})\mathcal{L}(\mathscr{P}|\mathscr{D})]\\
&=-\ell(\mathscr{P})-\sum_{\mathbf{y}\in \mathscr{D}}\ell(\mathscr{P}|\mathbf{y}).
\end{align}
\begin{exmp}
The samples drawn from the bivariate Gaussian distribution defined in Example \ref{exmp:mcmcmh} using Hamiltonian MCMC is depicted in Figure \ref{fig:mcmchmc}. The corresponding autocorrelation for both parameters using ``burn-in'' method are depicted in Figures \ref{fig:mcmchmc:w1corr} and \ref{fig:mcmchmc:w2corr}.
\end{exmp}
\section{Langevin Dynamics}\label{sec:LD}
As will be discussed in the next section, the Stochastic Gradient HMC works by considering Langevin dynamics on its momentum. The said dynamics extend the idea of the Newton's second law of motion. Originally, the second law proceeds as follows: let $\mathbf{f}$ be the force, $\mathbf{p}$ be the momentum, $m$ be the mass, $\mathbf{v}$ be the veclocity, and $\mathbf{a}$ be the acceleration, then
\begin{equation}
\mathbf{f}=\frac{\D \mathbf{p}}{\D t}=\frac{\D(m \mathbf{v})}{\D t}=m\frac{\D \mathbf{v}}{\D t}=m\mathbf{a}.
\end{equation}
The idea of Langevin dynamics is to take into account or at least approximate the effect of neglected degrees of freedom, and this is achieved by adding two force terms: one represents the frictional force, $\gamma \mathbf{v}^{\idxa}$; and the other represents the random force, $\mathbf{e}$. So that the Langevin equation is given below:
\begin{equation}
\frac{\D \mathbf{p}}{\D t}-\gamma\mathbf{v}^{\idxa}+\mathbf{e}=m\mathbf{a},
\end{equation}  
where the random force is assumed to have zero mean and is uncorrelated, i.e. $\mathbf{e}\sim\mathcal{N}(\mathbf{0}, \xi\mathbf{I})$.
\section{Stochastic Gradient HMC}
The discussion in this section is mainly based on \cite{tchen}. The idea is to apply the common practice in machine learning for speeding up the numerical computations in optimization problems. For very large dataset, especially in the era of Big data, the use of batch gradient descent can be very slow. Even for 100 observations only, the convergence can take time as illustrated in Example \ref{exmp:bgdslr}. And sometimes for multimodal surface function, the convergence might not fall into the global solution. To address this issue as presented in $\S$ \ref{sec:sgd}, is to consider stochastic gradient or minibatch stochastic gradient approach. That instead of taking single observation for updating the differential equation in the case of pure stochastic gradient approach or using all observations in the case of batch gradient, why not use only samples of the full dataset? The computational advantage of minibatch gradient over the two approaches is vectorization. Vectorization is computationally fast if it is possible for the algorithm. To formally begin, let $\tilde{\mathscr{D}}$ be the minibatch or sample of the full dataset $\mathscr{D}$. Then $\tilde{\mathscr{D}}\subseteq\mathscr{D}$, implies that
\begin{equation}
\nabla\tilde{\mathbb{U}}(\mathbf{w})=-\frac{|\mathscr{D}|}{|\tilde{\mathscr{D}}|}\sum_{\mathbf{x}\in\tilde{\mathscr{D}}}\nabla\log\p(\mathbf{x}|\mathbf{w})-\nabla\log\p(\mathbf{w}).
\end{equation}
The minibatch above is uniformly sampled from $\mathscr{D}$, and by doing so, the weight $\frac{|\mathscr{D}|}{|\tilde{\mathscr{D}}|}$ makes $\nabla\tilde{\mathbb{U}}(\mathbf{w})$ an estimate to $\nabla\mathbb{U}(\mathbf{w})$. The error of this estimate which in this case is known as the \textit{stochastic gradient noise}, is given by
\begin{equation}\label{eq:xi}
\nabla\tilde{\mathbb{U}}(\mathbf{w})-\nabla\mathbb{U}(\mathbf{w})=\boldsymbol{\xi}.
\end{equation}
Obviously, $\ev[\nabla\tilde{\mathbb{U}}(\mathbf{w})]=\nabla\mathbb{U}(\mathbf{w})$ hence
\begin{equation}
\ev[\nabla\tilde{\mathbb{U}}(\mathbf{w})-\nabla\mathbb{U}(\mathbf{w})]=\ev[\boldsymbol{\xi}]=\mathbf{0}.
\end{equation}
Let $\var[\boldsymbol{\xi}]=\boldsymbol{\mathfrak{A}}(\mathbf{w})$ be the variance-covariance matrix of the stochastic gradient noise, then by central limit theorem (CLT), $\boldsymbol{\xi}\sim\mathcal{N}(\mathbf{0},\boldsymbol{\mathfrak{A}}(\mathbf{w}))$. And therefore $\tilde{\mathbb{U}}(\mathbf{w})$ is approximated as follows:
\begin{equation}\label{eq:graduapprox}
\nabla\tilde{\mathbb{U}}(\mathbf{w})\approx\nabla\mathbb{U}(\mathbf{w})+\boldsymbol{\xi},\quad\boldsymbol{\xi}\sim\mathcal{N}(0,\boldsymbol{\mathfrak{A}}(\mathbf{w})).
\end{equation}
The equality in Equation (\ref{eq:xi}) is replaced with approximation in Equation (\ref{eq:graduapprox}) since $\boldsymbol{\xi}$ is now taken as a sample from a defined distribution, the Gaussian. And in effect, the momentum update of Algorithm \ref{algo:mcmch} line 9 now has a noise term added. That is, $\Delta\mathbf{p}=-\varepsilon\nabla\tilde{\mathbb{U}}(\mathbf{w})$, so that $\var[-\varepsilon\boldsymbol{\xi}]=\varepsilon^2\boldsymbol{\mathfrak{A}}(\mathbf{w})$ or $2\boldsymbol{\mathfrak{B}}(\mathbf{w})$ where $\boldsymbol{\mathfrak{B}}(\mathbf{w})=\frac{1}{2}\varepsilon \boldsymbol{\mathfrak{A}}(\mathbf{w})$ is the diffusion matrix. $\varepsilon^2$ is redefined in this case as $\varepsilon$, since it is just a constant. The resulting discrete time system can be viewed as an $\varepsilon$-discretization of the following continuous stochastic differential equation:
\begin{equation}\label{eq:diff1}
\frac{\D\mathbf{w}}{\D t}=\boldsymbol{\Sigma}^{-1}\mathbf{p}\quad\text{and}\quad
\frac{\D\mathbf{p}}{\D t}\approx-\nabla\mathbb{U}(\mathbf{w})+\boldsymbol{\xi}^{\idxa},
\end{equation}
where $\boldsymbol{\xi}^{\idxa} \sim \mathcal{N}(\mathbf{0},2\boldsymbol{\mathfrak{B}})$. For brevity, $\boldsymbol{\mathfrak{B}}(\mathbf{w})$ is now notated as $\boldsymbol{\mathfrak{B}}$ for the rest of the chapter. To gain some intuition, consider again the hockey puck analogy of $\S$ \ref{sec:hamiltoniandyn}. The dynamics is still the same but this time there is a presence of some random wind blowing. So the wind may blow the puck further away than expected. And due to this term, the preservation of the entropy under the Hamiltonian dynamics is not anymore satisfied. The following results are extracted from \cite{tchen}, please refer to the said article for the proof.
\begin{prop}
Let $\p_t(\mathbf{w},\mathbf{p})$ be the distribution of $(\mathbf{w}, \mathbf{p})$ at time $t$ with dynamics governed by Equation (\ref{eq:diff1}). Define the entropy of $\p_t$ as $\mathcal{H}(\p_t)=-\int_{\mathbf{w},\mathbf{p}}f(\p_t(\mathbf{w},\mathbf{p}))\D \mathbf{w}\D \mathbf{p}$, where $f(x)=x\ln x$. Assume $\p_t$ is a distribution with density and gradient vanishing at infinity. Furthermore, assume the gradient vanishes faster than $\frac{1}{\ln \p_t}$. Then, the entropy of $\p_t$ increases overtime with rate
\begin{equation}\label{eq:8}
\partial_t \mathcal{H}(\p_t(\mathbf{w},\mathbf{p}))=\int_{\mathbf{w},\mathbf{r}}f^{"}(\p_t)(\nabla_{\mathbf{p}}\p_t(\mathbf{w},\mathbf{p}))^{\mathrm{T}}\boldsymbol{\mathfrak{B}}(\mathbf{w})\nabla_{\mathbf{p}}\p_t(\mathbf{w},\mathbf{p})\D \mathbf{w}\D \mathbf{p}.
\end{equation}
Equation (\ref{eq:8}) implies that $\partial_t\mathcal{H}(\p_t(\mathbf{w},\mathbf{p}))\geq 0$ since $\boldsymbol{\mathfrak{B}}(\mathbf{w})$ is a positive semi-definite matrix.
\end{prop}
\begin{cor}
The distribution $\p(\mathbf{w}, \mathbf{p}|\mathscr{D})\propto \exp(-\mathbb{H}(\mathbf{w},\mathbf{p}))$ is no longer invariant under the dynamics of Equation (\ref{eq:diff1}).
\end{cor}
To address the problem above, a friction term is added to the equation. This introduces a correction step even before considering errors introduced by the discretization of the dynamical system. So that Equation (\ref{eq:diff1}) becomes
\begin{equation}\label{eq:diff2}
\frac{\D\mathbf{w}}{\D t}=\boldsymbol{\Sigma}^{-1}\mathbf{p}
\quad\text{and}\quad\frac{\D\mathbf{p}}{\D t}=-\nabla\mathbb{U}(\mathbf{w})-\boldsymbol{\mathfrak{B}}\boldsymbol{\Sigma}^{-1}\mathbf{p} + \boldsymbol{\xi}^{\idxa}.
\end{equation}
where $\boldsymbol{\xi}^{\idxa}\sim\mathcal{N}(\mathbf{0},2\boldsymbol{\mathfrak{B}})$. Referring again to \cite{tchen}, the posterior distribution described by the dynamics in the above equation is the unique stationary distribution. Equation (\ref{eq:diff2}) is also referred to us the \textit{second-order Langevin dynamics}, \textit{see} \S\,\ref{sec:LD}.
\begin{algorithm}[!t]
\caption{\it Stochastic Hamiltonian MCMC}
\label{algo:sghmc}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize Leap Frog parameters: $\varepsilon$ and $\tau$; the parameters of target distribution: $\mathscr{P}$; and the set of samples $\mathscr{S}=\{\boldsymbol{\theta}_r:\boldsymbol{\theta}_r\sim\mathbb{U}(\boldsymbol{\theta}), \forall r\}$\vspace{.2cm}
\item Define the Potential Energy (Target Distribution), $\mathbb{U}(\mathbf{w})$; and the Kinetic Energy (Auxiliary Distribution), $\mathbb{K}(\mathbf{p})$;\vspace{.2cm}
\item Define the gradient of these energies: $\nabla_{\mathbf{w}} \mathbb{U}$ and $\nabla_{\mathbf{p}}\mathbb{K}$;\vspace{.2cm}
\State Initialize estimate for $\hat{\boldsymbol{\mathfrak{B}}}(\mathbf{w})=\frac{\varepsilon}{2}\hat{\boldsymbol{\mathfrak{A}}}(\mathbf{w})$, and specify the matrix $\boldsymbol{\mathfrak{C}}$;\vspace{.2cm}
\item Set initial location $\mathbf{w}_{r = 0}(t = 0)$;\vspace{.2cm}
\For {$r\in\{0,\cdots,r_{\text{max}}\}$}\vspace{.2cm}
\State Draw initial momentum, $\mathbf{p}_{r}(t = 0)\sim\mathbb{K}(\mathbf{p})$;\vspace{.2cm}
\State Simulate Hamiltonian dynamics using Leap Frog:\vspace{.2cm}
\For {$t\in\{0,\cdots,\tau\}$}\vspace{.2cm}
\begin{align}
\Delta\mathbf{w}_r(t + \varepsilon)\triangleq&\varepsilon\nabla_{\mathbf{p}_r(t + 1)}\mathbb{K}(\mathbf{p}_r(t + 1)),\\[.2cm]
\Delta\mathbf{p}_r(t + \varepsilon)\triangleq&-\varepsilon\nabla_{\mathbf{w}_r(t)}\tilde{\mathbb{U}}(\mathbf{w}_r(t))-\boldsymbol{\mathfrak{C}}(\mathbf{w}_r(t))\boldsymbol{\Sigma}^{-1}\mathbf{p}+\boldsymbol{\xi}^{\idxb}\\
&\text{where }\boldsymbol{\xi}^{\idxb}\sim\mathcal{N}(\mathbf{0}, 2\varepsilon(\boldsymbol{\mathfrak{C}}-\boldsymbol{\mathfrak{B}}))
\end{align}
\EndFor\vspace{.2cm}
%\State Compute the new Hamiltonian $\mathbb{H}'(\mathbf{w}_r(\tau + \varepsilon),\mathbf{p}_r(\tau + \varepsilon))$;\vspace{.2cm}
%\State Compute the changes in total energy: $\Delta\mathbb{H} = \mathbb{H}' - \mathbb{H}$;\vspace{.2cm}
\State $\boldsymbol{\theta}_r\triangleq\mathbf{w}_r$\vspace{.2cm}
\EndFor\\\vspace{.2cm}
\Return $\mathscr{S}$
\end{algorithmic}
\end{algorithm}
\subsection{SGHMC in Practice}
The parameter $\boldsymbol{\mathfrak{B}}$ up to this point is assumed to be known. However, this is not the case in real scenario. So a remedy is to consider an estimate of $\boldsymbol{\mathfrak{B}}$ instead, denoted as $\hat{\boldsymbol{\mathfrak{B}}}$, and define a user-specified friction term $\boldsymbol{\mathfrak{C}}\succeq \hat{\boldsymbol{\mathfrak{B}}}$. That is $\boldsymbol{\mathfrak{C}}-\hat{\boldsymbol{\mathfrak{B}}}\succeq 0$ suggests that the matrix $\boldsymbol{\mathfrak{C}}-\hat{\boldsymbol{\mathfrak{B}}}$ is positive-semidefinite. 
So that the dynamics becomes
\begin{equation}
\label{eq:diff3}
\frac{\D\mathbf{w}}{\D t}=\boldsymbol{\Sigma}^{-1}\mathbf{p}
\quad\text{and}\quad\frac{\D\mathbf{p}}{\D t}=-\nabla\tilde{\mathbb{U}}(\mathbf{w})-\boldsymbol{\mathfrak{C}}\boldsymbol{\Sigma}^{-1}\mathbf{p}+\boldsymbol{\xi}^{\idxb}.
\end{equation}
where $\boldsymbol{\xi}^{\idxb}\sim\mathcal{N}(\mathbf{0},2(\boldsymbol{\mathfrak{C}}-\hat{\boldsymbol{\mathfrak{B}}}))$. This results into what is known as the \textit{stochastic gradient Hamiltonian monte carlo} (SGHMC) algorithm defined in Algorithm \ref{algo:sghmc}.

As mentioned above, $\hat{\boldsymbol{\mathfrak{B}}}$ is an estimate for $\boldsymbol{\mathfrak{B}}$ since the latter is not known in practice. And as suggested by \cite{tchen}, $\hat{\boldsymbol{\mathfrak{B}}}$ can be set to $\mathbf{0}_{n\times n}$ as its simplest estimate, and in effect, Equation (\ref{eq:diff3}) will still preserve the entropy since the expression is now governed by controllable matrix $\boldsymbol{\mathfrak{C}}$. Further, using the fact that $\boldsymbol{\mathfrak{B}}=\frac{1}{2}\varepsilon \boldsymbol{\mathfrak{A}}$ then $\hat{\boldsymbol{\mathfrak{B}}}=\frac{1}{2}\varepsilon \hat{\boldsymbol{\mathfrak{A}}}$. And as $\varepsilon\to0$, $\boldsymbol{\mathfrak{B}}=\mathbf{0}_{n\times n}$. Another option is to estimate $\boldsymbol{\mathfrak{A}}$ using \textit{empirical} Fisher information as in \cite{sahn}.
\subsection{Empirical Fisher Information}
Fisher information measures how much information about the parameter is obtained from the data. Intuitively, if $\mathbf{w}$ is the parameter of interest, then the log-likelihood, $\ell(\mathbf{w}|\mathscr{D})$, is at its maximum if $\mathbf{w}=\mathbf{w}_0$ where $\mathbf{w}_0$ is the true value of the parameter. Or equivalently, the stationary point such that $\nabla_{\mathbf{w}}\ell(\hat{\mathbf{w}}|\mathscr{D})\set 0$, is at $\hat{\mathbf{w}}=\mathbf{w}_0$. For simplicity, consider the case of one-dimensional parameter $w$, if the probability of an event is large, i.e. $\ell(w|\mathscr{D})>c$, $\exists\, c>0$, or $[\nabla_{w}\ell(w|\mathscr{D})]^2\approx 0$, then this is expected. That is, little information about the parameter $w$ is gained. But if the $\ell(w|\mathscr{D})\approx 0$, or $[\nabla_{w}\ell(w|\mathscr{D})]^2>c$, $\exists\, c> 0$, then this tells new story about the characteristics of the parameter of interest. So the Fisher information in general about $\mathbf{w}$ is given by
\begin{equation}
\mathbb{I}(\mathbf{w})\triangleq\ev_{\mathscr{D}}\left\{[\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})][\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})]^{\mathrm{T}}\right\}.
\end{equation}
And obviously, $\ev_{\mathscr{D}}[\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})] = 0$. Finally, the \textit{empirical} Fisher information (\cite{sahn}) is defined as follows:
\begin{equation}\nonumber
\boldsymbol{\mathfrak{A}}(\mathbf{w},\mathscr{D})=\frac{1}{n-1}\sum_{i=1}^{n}\left\{\left[\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})-\overline{\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})}\right]\left[\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})-\overline{\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})}\right]^{\mathrm{T}}\right\},
\end{equation}
where $\overline{\nabla_{\mathbf{w}}\ell(\mathbf{w}|\mathscr{D})}=\displaystyle\frac{1}{n}\sum_{i=1}^n\nabla_{\mathbf{w}}\ell(\mathbf{w}|x_i)$.

}
