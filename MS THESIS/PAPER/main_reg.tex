\chapter{\normalsize MAIN RESULTS}\label{ch:HMM}
{\baselineskip=2\baselineskip
\lettrine{H}{\,ighlights of this} paper are presented in this chapter. Starting with the discussion on the application of Metropolis-Hasting for Bayesian regression and then using Hamiltonian Monte Carlo. The posterior distribution of these MCMCs are then compared to the exact solution of the Bayes' Rule.
\begin{equation}
\p(x)=\frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(\frac{(x-\mu)^2}{2\sigma^2}\right), \quad -\infty<x<\infty; 
\end{equation}
\section{Bayesian Regression using Metropolis-Hasting}
%\begin{exmp}[\it ]
%\label{exmp:brmh}
In Example \ref{exmp:sblbs}, the Bayesian approach to linear regression were shown to be effective in estimating the parameters of the model. Unfortunately, most of the interesting models (as emphasized already in this paper), often have intractable posterior distribution due to the normalizing constant that is often difficult to integrate. Assuming that, that is the case for the posterior of the weights in Example \ref{exmp:sblbs}, then MCMC method is the solution to it. In particular, the parameter estimation is computed as follows using the Metropolis-Hasting algorithm:
\begin{figure}[!t]
%\begin{tabular}{p{.5\textwidth}p{.5\textwidth}}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p39}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p41}
%\caption{Fitted Line.}
\end{subfigure}\\
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p40}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p42}
\end{subfigure}
%\end{tabular}
\caption[Metropolis-Hasting Samples from Posterior Distribution of $\mathbf{w}$]{\it Metropolis-Hasting Samples from Posterior Distribution of $\mathbf{w}$.}
\label{fig:exmp:bayeslinregmh}
\end{figure}

The log likelihood function defined in Equation (\ref{eq:likelihood:blreg}), is given by
%\iffalse
\begin{align}
\mathcal{\ell}(\mathbf{w}|\mathbf{y}) \triangleq \log\p(\mathbf{y}|\mathbf{w})&=
\sum_{k=1}^K\log\frac{1}{\sqrt{2\pi\alpha^{-1}}}-\sum_{k=1}^{K}\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}\\
&=-K\log\sqrt{2\pi\alpha^{-1}}-\sum_{k=1}^{K}\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}.
\end{align}
%\fi
For this example, log transformation is used since it is computationally accurate. This follows from the fact that the likelihood is computed by multiplying small probabilities, which in return gives a very small value. And because of that, computer softwares often do rounding of the small numbers, leading to error in processing. So that the log \textit{a posteriori} is given by
\begin{equation}
\label{eq:logpostblrmcmc}
\log \p(\mathbf{w}|\mathbf{y}) = \ell(\mathbf{w}|\mathbf{y}) + \log\p(\mathbf{w}) - \log\p(\mathbf{y}).
\end{equation}
The normalizing term, $\log\p(\mathbf{y})$, in Equation (\ref{eq:logpostblrmcmc}) is not necessarily required in looping through the Metropolis-Hasting  algorithm. This is mentioned in Section \ref{sec:metropolishasting}, that this constant will be cancelled out in the acceptance criterion of the said algorithm. Speaking of the acceptance criterion, the log transformation above must be adjusted to agree with the equation in line 4 of Algorithm \ref{algo:mcmcmh}, and is done by enclosing it with exponential function to remove the log transformation. The chain of values of both parameters sampled from the posterior distribution is shown in Figure \ref{fig:exmp:bayeslinregmh}. The proposal function is a Gaussian with mean as the chain values and with unit variance. Using this proposal, there is a clear evidence of high rejection rate of the proposed samples. This is due to the step function exhibited by the chain values of the parameter estimates, suggesting that previous sample is likely to be retained at the next iteration.

Using the estimates of the parameters, the predicted values along with the 500 samples (extracted every 19th interval from the 9500 samples after burn-in) from the posterior distribution are shown in Figure \ref{fig:seqbayeslearnexmp:abc}.
%\end{exmp}
\begin{figure}[!t]
%\begin{tabular}{p{.5\textwidth}p{.5\textwidth}}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p43}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p45}
%\caption{Fitted Line.}
\end{subfigure}\\
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p44}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p46}
\end{subfigure}
\caption[Hamiltonian Monte Carlo Samples from Posterior Distribution of $\mathbf{w}$]{\it Hamiltonian Monte Carlo Samples from Posterior Distribution of $\mathbf{w}$.}
\label{fig:exmp:bayeslinreghmc}
\end{figure}
\begin{prop}\label{prop:gradpotential}
Let $\mathscr{D}=\left\{(\mathbf{x},\mathbf{y})\right\}$ be the data such that $\mathbf{y}$ is modelled by a Gaussian function with mean $\mathbf{w}^{\text{T}}\mathbf{x}$, $\mathbf{w}\in\mathbb{R}^d$, and variance $\alpha^{-1}=\frac{1}{\sigma^2}$. If $\mathbf{w}\sim\mathcal{N}_d(\mathbf{0},\beta^{-1}\mathbf{I})$, where $\beta^{-1}\in\mathbb{R}_{+}$, then having a posterior defined in Equation (\ref{eq:bayesrulepost}), the gradient of $- \log\p(\mathbf{w}|\mathbf{y})$ is given by following equation
\begin{equation}
-\alpha\sum_{k=1}^{K}(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)\mathbf{x}_k  +\beta\mathbf{w}.
\end{equation}
\end{prop}
\begin{proof} Consider the following:
\begin{align}
\frac{\D}{\D\mathbf{w}}[- \log\p(\mathbf{w}|\mathbf{y})] &\triangleq - \frac{\D}{\D\mathbf{w}}\left[\ell(\mathbf{w}|\mathbf{y}) + \log\p(\mathbf{w}) - \log\p(\mathbf{y})\right]\\
&=-\left[\frac{\D}{\D\mathbf{w}}\ell(\mathbf{w}|\mathbf{y}) + \frac{\D}{\D\mathbf{w}}\log\p(\mathbf{w})\right]
\end{align}
where from Equation (\ref{eq:likelihood:blreg})
\begin{align}
\frac{\D}{\D\mathbf{w}}\ell(\mathbf{w}|\mathbf{y})&=\frac{\D}{\D\mathbf{w}}\log\left\{\left(\frac{\alpha}{2\pi}\right)^{K/2}\exp\left[-\sum_{k=1}^K\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}\right]\right\}\\
&=-\frac{\D}{\D\mathbf{w}}\sum_{k=1}^K\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}=\alpha\sum_{k=1}^{K}(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)\mathbf{x}_k\\
&=\left[
\begin{array}{c}
\alpha\sum_{k=1}^{K}(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)\\
\alpha\sum_{k=1}^{K}(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)x_k
\end{array}
\right]
\end{align}
and the derivative of the prior given in Equation \ref{eq:wpriori} with log transformation is given by
\begin{align}
\frac{\D}{\D\mathbf{w}}\log\p(\mathbf{w})=-\frac{\beta}{2}\frac{\D}{\D\mathbf{w}}\mathbf{w}^{\text{T}}\mathbf{w}=-\beta\mathbf{w}.
\end{align}
And that completes the proof.
\end{proof}
\section{Bayesian Regression using Hamiltonian Monte Carlo}
Alternatively, sampling from posterior distribution using Hamiltonian Monte Carlo is done as follows: Recall that the target distribution which is the posterior is defined by the potential energy. So that taking into account the log transformation justified in Example \ref{exmp:brmh}, the target distibution is given by
\begin{align}
\mathbb{U}(\mathbf{w}) &\triangleq - \log\p(\mathbf{w}|\mathbf{y})\\
&=-\ell(\mathbf{w}|\mathbf{y}) - \log\p(\mathbf{w}) + \log\p(\mathbf{y})
\label{eq:potential:posterior}
\end{align}
The normalizing term, $\log\p(\mathbf{y})$, can be ignored since later in the computation this constant will be cancelled out as already shown in subsection of Section \ref{sec:hamiltoniandyn}. So that using Proposition \ref{prop:gradpotential}, the gradient of $\mathbb{U}(\mathbf{w})$ is $-\alpha\sum_{k=1}^{K}(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)\mathbf{x}_k  +\beta\mathbf{w}$.
\begin{figure}[!t]
%\begin{tabular}{p{.3\textwidth}p{.3\textwidth}p{.3\textwidth}}
%\multicolumn{1}{c}{Likelihood}&\multicolumn{1}{c}{Prior/Posterior}&\multicolumn{1}{c}{Data Space}\\
\begin{subfigure}[b]{.33\textwidth}
\includegraphics[width=\textwidth]{p47}
\caption{Bayes' Rule.}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
\includegraphics[width=\textwidth]{p48}
\caption{M-H MCMC.}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
\includegraphics[width=\textwidth]{p49}
\caption{HMC MCMC.}
\end{subfigure}
%\end{tabular}
\caption[Predicted and Sample Bayesian Regression Lines]{\it Predicted Values and Samples of Regression Lines using Different Bayesian Methods. The left plot is based on Bayes' rule with bivariate Gaussian derived posterior having parameters defined in Equations (\ref{eq:sigmablrgauss}) and (\ref{eq:mublrgauss}). (b) is computed using Metropolis-Hasting, while (c) is obtained from Hamiltonian Monte Carlo sampling.}
\label{fig:seqbayeslearnexmp:abc}
\end{figure}
The histogram and chain values of the parameters are given in Figure \ref{fig:exmp:bayeslinreghmc}. Unlike Metropolis-Hasting algorithm, there is no clear evidence (subjectively from the plot) of high rejection rate, suggesting that the samples have a good mixing characteristics.

Using the estimates of the parameters, the predicted values along with the 500 samples (extracted every 19th interval from the 9500 samples after burn-in) from the posterior distribution are shown in Figure \ref{fig:seqbayeslearnexmp:abc}. 
%\end{exmp}

}