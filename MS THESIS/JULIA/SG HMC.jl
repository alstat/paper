"""
STOCHASTIC GRADIENT HAMILTONIAN MONTE CARLO

The following codes explore the use of SGHMC for Bayesian Inference
"""
using PyPlot
using Distributions
using StatsBase: autocor

immutable SGHMC
  dU      ::Function
  dK      ::Function
  dKΣ     ::Array{Float64}
  C       ::Array{Float64}
  V       ::Array{Float64}
  init_est::Array{Float64}
  d       ::Int64
end

function sghmc(Parameters::SGHMC;
  leapfrog_params::Dict{Symbol, Real} = Dict([:ɛ => .05, :τ => 20]),
  set_seed::Int64 = 123,
  r::Int64 = 10000)

  dU, dK, dKΣ, C, V, w, d = Parameters.dU, Parameters.dK, Parameters.dKΣ, Parameters.C, Parameters.V, Parameters.init_est, Parameters.d
  ɛ, τ = leapfrog_params[:ɛ], leapfrog_params[:τ]

  x = zeros(r - 1, d); w = zeros(d, 1)
  B = .5 * V * ɛ
  D = sqrt(2 * (C - B) * ɛ)
  if size(B) != size(C)
    error("C and V should have the same dimension.")
  else
    if sum(size(B)) > 1
      if det(B) > det(C)
        error("ɛ is too big. Consider decreasing it.")
      end
    else
      if det(B[1]) > det(C[1])
        error("ɛ is too big. Consider decreasing it.")
      end
    end
  end

  for i in 1:(r - 1)
    p = randn(d, 1)

    for j in 1:τ
      p = p - dU(w) * ɛ - C * inv(dKΣ) * p + D * randn(d, 1);
      w = w + dK(p) * ɛ;
    end

    x[i, :] = w
  end

  return x
end


"""
BAYESIAN LINEAR REGRESSION
"""
w0 = -.3; w1 = -.5; stdev = 5.;

# Define data parameters
alpha = 1 / stdev; # for likelihood

# Generate Hypothetical Data
n = 200000;
x = rand(Uniform(-1, 1), n);
A = [ones(length(x)) x];
B = [-.8; -.1];
f = A * B
y = f + rand(Normal(0, alpha), n)

# Define Hyperparameters
Imat = diagm(ones(2), 0)
b = 2 # for prior
b1 = (1 / b)^2 # Square this since in Julia, rnorm uses standard dev

mu = zeros(2) # for prior
s = b1 * Imat # for prior

function dPotential(theta::Array{Float64}; alpha::Float64 = 1/5., b::Float64 = 2.)
  [-alpha * sum(y - (theta[1] + theta[2] * x));
   -alpha * sum((y - (theta[1] + theta[2] * x)) .* x)] + b * theta + randn(2,1)
end

dKinetic(p::AbstractArray{Float64}; Σ::Array{Float64} = eye(length(p))) = inv(Σ) * p;
SGHMC_object = SGHMC(dPotential, dKinetic, [[1 .5]; [.5 1]], eye(2), eye(2), [0; 0], 2.);

@time weights1 = sghmc(SGHMC_object, leapfrog_params = Dict([:ɛ => .009, :τ => 20]))
mapslices(mean, weights1, [1])

histogram(weights[:, 1])
histogram(weights[:, 2])

xyplot(weights[:, 1], weights[:, 2])

barplot(autocor(weights[:, 1]))
barplot(autocor(weights[:, 2]))


"""
EXAMPLE: Sampling from One Dimensional Function
"""

dU(x::AbstractArray{Float64, 1}; μ::AbstractArray{Float64, 1} = [10.; -10.], Σ::AbstractArray{Float64, 2} = [[1.5^2  1.5*1.35*.5]; [1.5*1.35*.5 1.35^2]]) = (Σ)^(-1) * (x - μ);
dK(p::AbstractArray{Float64, 1}) = p;
C = [[3. .2]; [.2 3.]];
V = repmat([0], 2, 2);
μ = [10.; -10.]; Σ = [[1.5^2  1.5*1.35*.5]; [1.5*1.35*.5 1.35^2]]
rand(MultivariateNormal(repmat([0.], d, 1) |> vec, C))

Parameters = SGHMC(dU, dK, C, V, 2)
leapfrog_params = Dict([:ɛ => .1, :τ => 20])
set_seed = 123
r = 10000
dU, dK, C, V, d = Parameters.dU, Parameters.dK, Parameters.C, Parameters.V, Parameters.d
ɛ, τ = leapfrog_params[:ɛ], leapfrog_params[:τ]
x = repmat([0.], r, d);
if d == 1
  xNew = 0
else
  xNew = repmat([0], d, 1)
end
i = 2
M = eye(d)
for i in 2:(r - 1)
  p = rand(MultivariateNormal(zeros(d), M))

  for j in 1:τ
    if sum(V) == 0
      p = p - ɛ * dU(xNew) - ɛ * C * M^(-1) * p + ɛ * rand(MultivariateNormal(zeros(d), 2 * (C - B)))
    else
      p = p - ɛ * (dU(xNew) - rand(MultivariateNormal(zeros(d), .5 .* V), d)) - ɛ * C * p +  ɛ * rand(MultivariateNormal(zeros(d), 2 * (C - B)))
    end
    xNew = xNew + ɛ * dK(p)
  end

  x[i, :] = xNew
end
x
xyplot(x[:, 1], x[:, 2])
sghmc(sghmc_params)

nsample = 80000;
xStep = 0.01;
m = 1; # dK
C = 3;
dt = 0.1;
nstep = 50;
V = 4;

U(x) = -2(x.^2) + x.^4;
dU(x) = -4x + 4(x.^3);
dU2(x) = -4x + 4(x.^3) + 2*rand(Normal());
K(p) = (p * p) / 2;
dK(p) = p;
H(x, p) = U(x) + K(p);



x5 = Array{Float64, 1}(); xNew = 0
for n in 1:(r - 1)
  p = rand(Normal()) * sqrt(1);
  B = 0.5 * V * ɛ;
  D = sqrt(2 * (C - B) * ɛ);

  for i = 1:τ
      p = p - dU2(xNew) * ɛ  - p * C * ɛ  + rand(Normal()) * D;
      xNew = xNew + dK(p) * dt;
  end

  push!(x5, xNew)
end


# SGHMC FOR GAUSSIAN DISTRIBUTION


y = randn(10)

immutable T
  w::Function
end


dw(p, y) = sum(y)^p

function f1(a::T)
  w = a.w

  out = Float64[]
  for i in 1:10
    push!(out, w(i, y))
  end

  return out
end

t1 = T(dw)

f1(t1)
