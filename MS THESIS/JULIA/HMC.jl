"""
HAMILTONIAN MONTE CARLO

The following codes explore the use of HMC for Bayesian Inference.
"""
using Distributions: Normal, Uniform
using StatsBase: autocor
using PyPlot

immutable HMC
   U::Function
   K::Function
  dU::Function
  dK::Function
   d::Int64
end

function hmc(Energies::HMC;
  leapfrog_params::Dict{Symbol, Real} = Dict([:ɛ => .3, :τ => 20]),
  set_seed::Int64 = 123,
  r::Int64 = 10000)

  """
  Energies - HMC type which contains functions for
             Potential and Kinetic Energies, including their
             corresponding gradients.
  leapfrog_params - parameters of the leap frog method
  r - number of sampling iteration
  """
  U, K, dU, dK, d = Energies.U, Energies.K, Energies.dU, Energies.dK, Energies.d
  ɛ, τ = leapfrog_params[:ɛ], leapfrog_params[:τ]
  H(x::AbstractArray{Float64, 1}, p::AbstractArray{Float64, 1}) = U(x) + K(p)

  if typeof(set_seed) == Int64
    srand(set_seed)
  end

  x = zeros(r, d);
  x[1, :] = zeros(d, 1);

  for i in 1:(r - 1)
    xNew = x[i, :]
    p = randn(length(xNew))
    oldE = U(xNew) + K(p)

    for j in 1:τ
      p = p - (ɛ / 2) * dU(xNew)
      xNew = xNew + ɛ * dK(p)
      p = p - (ɛ / 2) * dU(xNew)
    end

    newE = U(xNew) + K(p)
    dE = newE - oldE

    if dE[1] < 0
      x[i + 1, :] = xNew
    elseif rand(Uniform()) < exp(-dE)[1]
      x[i + 1, :] = xNew
    else
      x[i + 1, :] = x[i, :]
    end
  end

  return x
end

"""
EXAMPLE: Sampling from Gaussian Distribution
"""
# Define the Functions
Potential(x::AbstractArray{Float64}; μ::AbstractArray{Float64} = zeros(2), Σ::AbstractArray{Float64} = [[1  .9]; [.9 1]]) = (x - μ)' * (Σ)^(-1) * (x - μ);
dPotential(x::AbstractArray{Float64}; μ::AbstractArray{Float64} = zeros(2), Σ::AbstractArray{Float64} = [[1  .9]; [.9 1]]) = (Σ)^(-1) * (x - μ);
Kinetic(p::AbstractArray{Float64}) = (p' * p) / 2;
dKinetic(p::AbstractArray{Float64}) = p;

HMC_object = HMC(Potential, Kinetic, dPotential, dKinetic, 2);

# Sample
@time weights = hmc(HMC_object, r = 50000);

# Plot the Samples
mapslices(mean, weights, [1])

histogram(weights[:, 1])
histogram(weights[:, 2])

xyplot(weights[:, 1], weights[:, 2])

barplot(autocor(weights[:, 1]))
barplot(autocor(weights[:, 2]))

"""
EXAMPLE: Bayesian Linear Regression
"""
srand(123);
w0 = -.3; w1 = -.5; stdev = 5.;

# Define data parameters
alpha = 1 / stdev; # for likelihood

# Generate Hypothetical Data
n = 20000
x = rand(Uniform(-1, 1), n);
A = [ones(length(x)) x];
B = [w0; w1];
f = A * B
y = f + rand(Normal(0, alpha), n)

b = 2. # for prior

U(theta::Array{Float64}) = - logpost(theta)
dU(theta::Array{Float64}) = [-alpha * sum(y .- (theta[1] + theta[2] * x));
                             -alpha * sum((y .- (theta[1] + theta[2] * x)) .* x)] .+ b * theta

K(p::Array{Float64}; Σ = eye(length(p))) = (p' * inv(Σ) * p) / 2
dK(p::Array{Float64}; Σ = eye(length(p))) = inv(Σ) * p

HMC_object2 = HMC(U, K, dU, dK, 2);

# Sample
@time weights = hmc(HMC_object2, leapfrog_params = Dict([:ɛ => .009, :τ => 20]));
weights

# Plot the Samples
mapslices(mean, weights, [1])

histogram(weights[:, 1])
histogram(weights[:, 2])

xyplot(weights[:, 1], weights[:, 2])

barplot(autocor(weights[:, 1]))
barplot(autocor(weights[:, 2]))
