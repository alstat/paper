\chapter{\normalsize BASIC DEFINITIONS AND RESULTS}\label{ch:bcd}
{\baselineskip=2\baselineskip
\lettrine{B}{\,asic concepts, definitions} and results for Bayesian regression and Bayesian classification are the main focus of this chapter. It is also intended to give the reader an introduction for the use of mathematical optimization --- a frequentist approach to estimating the parameters with no closed form solution. And some insights to the general applicability of these tools.
\begin{defn}[Entropy]
Let $X$ be the event, then according to \textit{information theory} the amount of information extracted from this event is quantified as follows:
\begin{equation}
\mathbb{I}(X)=-\log \p(X).
\end{equation}
And for events $X_1,\cdots,X_N$, the average of this information is known as entropy and is given by:
\begin{equation}
\mathcal{H}(\mathbf{X})=-\sum_{i=1}^N\p(X_i)\log \p(X_i).
\end{equation}
\end{defn}

\begin{remark}
For continuous variable, simply replace the summation to integration.
\end{remark}
\section{Gradient Descent (GD)}
There are several ways to numerically estimate the parameters of the model using mathematical programming. One popular algorithm that is very common in Machine Learning is the \textit{gradient descent} algorithm. It uses gradient and requires tuning of the step size as seen in the Algorithm \ref{algo:GD}.\vspace{.4cm}

\begin{algorithm}
\caption{\it Gradient Descent}
\label{algo:GD}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize $\hat{\mathbf{w}}_{r},r=0$\vspace{.2cm}
\While{$\lVert \hat{\mathbf{w}}_{r}-\hat{\mathbf{w}}_{r+1}\rVert > \nu$}\vspace{.2cm}
\State $\hat{\mathbf{w}}_{r+1}\leftarrow \hat{\mathbf{w}}_{r} - \eta\nabla\ein(\hat{\mathbf{w}}_r)$\vspace{.2cm}
\State $r\leftarrow r + 1$\vspace{.2cm}
\EndWhile\\\vspace{.2cm}
\Return $\hat{\mathbf{w}}_{r}$ and $r$.
\end{algorithmic}
\end{algorithm}
\vspace{-.3cm}
Suppose $\nabla\ein(\hat{\mathbf{w}}_r)$ is the gradient of the cost function at the $r$th iteration. In Chapter \ref{ch:bayesmlp}, $\ein$ is defined as the ``in-sample error'' or the error in the training data. $\eta$ is the \textit{learning-rate} parameter of the algorithm, and $\nu$ is the precision parameter. As an illustration, consider the following examples:
\begin{exmp}
Suppose the loss function is given by
\begin{equation}\label{eq:errgd1}
\ein(w)\triangleq w^4 - 3w^3 + 2.
\end{equation}
Differentiating with respect to $w$ gives $\ein'(w)=4w^3-9w^2$. Let the initial guess be $\hat{w}_0=.1$ and let $\eta=.01$ with $\nu=.00001$. Then $\nabla\ein(\hat{w}_0)=\ein'(\hat{w}_0)=-0.086$, so that $\hat{w}_1\triangleq\hat{w}_0-.01(-0.086)=0.10086$. And $|\hat{w}_1 - \hat{w}_0| = 0.00086> \nu$. Repeat the process until $\exists \,r,\,|\hat{w}_r - \hat{w}_r| \ngtr \nu$. It will turn out that 173 iterations are needed to satisfy the desired inequality, and the plot of it is in Figure \ref{fig:f1}.
\end{exmp}
\begin{figure}[!t]
\includegraphics[width = \linewidth]{p0.pdf}
\caption[Gradient Descent on Equation (\ref{eq:errgd1})]{\it Gradient Descent on Equation (\ref{eq:errgd1}).}
\label{fig:f1}
\end{figure}
In practice, there are hundreds to millions of data points that needs to be summarized. So at each iteration, parameter updating is performed \textit{after} the presentation of \textit{all} the training examples that constitute an \textit{epoch} --- one complete presentation of the entire training set during the learning process (\cite{Haykin1998}). In this setting, GD is sometimes called \textit{batch gradient descent} (BGD).
\section{Stochastic Gradient Descent (SGD)}\label{sec:sgd}
An alternative to BGD is SGD or \textit{stochastic gradient descent}. By definition, BGD is clearly computationally expensive especially for big data. SGD on the other hand, updates the parameter using only one observation for every iteration, which is a lot faster. Also, since GD is prone to local minimum, so thus BGD. This is guaranteed especially for high dimensional nonlinear error surface function. In this case, there is a need to play on different starting values to get closer to the global minimum. This problem, however, is well handled by SGD, since it is stochastic. That is in each epoch the data are randomized. But unlike BGD, SGD will not guarantee exact value of the global minimum, instead it will only stay around the global minimum, which is reasonable for the price of speed.\vspace{.4cm}

\begin{algorithm}[!t]
\caption{\it Stochastic Gradient Descent}
\label{algo:SGD}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize $\hat{\mathbf{w}}_{r},r=0$\vspace{.2cm}
\While{$\lVert \hat{\mathbf{w}}_{r}-\hat{\mathbf{w}}_{r+1}\rVert > \nu$}\vspace{.2cm}
\State Randomize the data set ($\mathbf{x}_k,\mathbf{y}_k$) with respect to $k$.\vspace{.2cm}
\For{$k \in\{1,\cdots, K\}$}\vspace{.2cm}
\State Update the parameters
\begin{align}
\hat{\mathbf{w}}_{r}&\triangleq \hat{\mathbf{w}}_{r} - \eta\nabla\mathrm{e}(h(\mathbf{x}_k,\mathbf{w}),\mathbf{y}_k)\\
&=\hat{\mathbf{w}}_{r} - \eta\frac{\partial}{\partial\mathbf{w}}\left\{\frac{1}{2}[h(\mathbf{x}_k, \mathbf{w})-\mathbf{y}_k]^2\right\}
\end{align}
\State Compute $\mathrm{e}(h(\mathbf{x}_k,\hat{\mathbf{w}}_r),\mathbf{y}_k)$\vspace{.2cm}
\EndFor\vspace{.2cm}
\State $\hat{\mathbf{w}}_{r+1}\triangleq \hat{\mathbf{w}}_{r}$\vspace{.2cm}
\State Compute the cost function function
\begin{equation}
\ein(h)=\frac{1}{K}\sum_{k=1}^K\mathrm{e}(h(\mathbf{x}_k,\hat{\mathbf{w}}_{r+1}),\mathbf{y}_k)
\end{equation}
\EndWhile\vspace{.2cm}
\item\Return $\hat{\mathbf{w}}_{r}$ and $r$.
\end{algorithmic}
\end{algorithm}
To illustrate, the following results formulates the batch and stochastic gradient descent for simple linear regression model.
\begin{defn}\label{defn:slr}
Let $f:\mathcal{X}\rightarrow\mathcal{Y}$ be the function of interest such that the hypothesis function $h:\mathcal{X}\rightarrow\mathcal{Y}$ approximates the function $f$, then the simple linear regression model with intercept is defined as
\begin{align}
h(x_k,\boldsymbol{\beta})&\triangleq\beta_0+\beta_1x_k,\quad k=1,\cdots,K\\
&=\mathbf{X}\boldsymbol{\beta},
\end{align}
where $\mathbf{X}$ is the data matrix and $\boldsymbol{\beta}$ is the vector of parameters.
\end{defn}
\begin{prop}
Let the squared error cost function be the objective function of the model in Definition \ref{defn:slr}, that is
\begin{align}
\ein(h)&\triangleq\frac{1}{2K}\sum_{k=1}^{K}[h(x_k,\boldsymbol{\beta})-y_k]^2=\frac{1}{2K}\sum_{k=1}^{K}(\beta_0+\beta_1x_k-y_k)^2\\
&=\frac{1}{2K}\lVert\mathbf{X}\boldsymbol{\beta}-\mathbf{y}\rVert^2.
\end{align}
Then the gradient vector of $\ein(h)$ is
\begin{equation}
\nabla\ein(h)=\frac{1}{K}\mathbf{X}^{\text{T}}(\mathbf{X}\boldsymbol{\beta}-\mathbf{y}).
\end{equation}
\end{prop}
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p5}
\caption{BGD on Loss Function Surface.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p6}
\caption{Loss Function under BGD.}
\end{subfigure}
\end{minipage}
\caption[Batch Gradient Descent on SLR Loss Function]{\it Batch Gradient Descent on SLR Loss Function.}
\label{fig:slrbgd}
\end{figure}
\begin{proof}
From matrix calculus,
\begin{align}
\frac{\partial}{\partial\boldsymbol{\beta}}\ein(h)&=\frac{\partial}{\partial\boldsymbol{\beta}}(\mathbf{X}\boldsymbol{\beta}-\mathbf{y})\frac{\partial}{\partial\ein(h)}\ein(h)\\
&=\mathbf{X}^{\text{T}}\frac{1}{K}(\mathbf{X}\boldsymbol{\beta}-\mathbf{y})
\end{align}
which is equivalent to
\begin{equation}
\frac{\partial}{\partial\boldsymbol{\beta}}\ein(h)=\left[\begin{array}{c}
\frac{\partial}{\partial\beta_0}\ein(h)\\[.3cm]
\frac{\partial}{\partial\beta_1}\ein(h)
\end{array}\right]=\left[\begin{array}{c}
\frac{1}{K}\sum_{k=1}^{K}(\beta_0+\beta_1x_k-y_k)\\[.3cm]
\frac{1}{K}\sum_{k=1}^{K}(\beta_0+\beta_1x_k-y_k)x_k\\
\end{array}\right].
\end{equation}
\end{proof}
As mentioned in Chapter 1, this thesis aims to provide codes for all necessary analyses using Julia, Python and R. However, for purpose of clarity and the fact that Julia is a new programming language for fast scientific computing, all necessary codes for illustration will be written in Julia.
\begin{exmp}\label{exmp:bgdslr}
Consider the following simulated data:\vspace{-.9cm}
\begin{singlespace}
\begin{verbatim}
using Distributions
srand(12345)

x = rand(Normal(15, 2), 100)
beta = [3.4; .75]
err = rand(Normal(), 100)
X = [[1 for i in 1:size(x)[1]] x]
y = X * beta + err
\end{verbatim}
\end{singlespace}
\vspace{-.4cm}
\noindent The true parameter value set in the code is $\boldsymbol{\beta}=\left[\begin{array}{c}3.4\\.75\end{array}\right]$. Suppose the precision parameter $\nu=.0001$ with learning rate $\eta=.002$ and initial guess $\hat{\boldsymbol{\beta}}_{0}=\left[\begin{array}{c}-3\\-3\end{array}\right]$. Applying batch gradient descent, the final estimate is $\hat{\boldsymbol{\beta}}_{19,544}=\left[\begin{array}{c} 0.2200854\\0.9549819\end{array}\right]$ at 19,544th iteration, see Figure \ref{fig:slrbgd}.
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p7}
\caption{SGD on Loss Function Surface.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p8}
\caption{Loss Function under SGD.}
\end{subfigure}
\end{minipage}
\caption[Stochastic Gradient Descent on SLR Loss Function]{\it Stochastic Gradient Descent on SLR Loss Function.}
\label{fig:slrsgd}
\end{figure}
On the other hand, it takes 4,614 iterations for the stochastic gradient descent to converge with estimate $\hat{\boldsymbol{\beta}}_{4,614}=\left[\begin{array}{c} 2.732653\\0.814412\end{array}\right]$. In this case, SGD performs well compared to BGD in terms of speed in convergence. This is not always true, however, since SGD shuffles the data randomly in every epoch. So there are times when it takes more iterations than BGD, and sometimes it will not converge, and in practice maximum number of iterations is specified to stop the algorithm. The ordinary least square (OLS) estimate is $\hat{\boldsymbol{\beta}}_{\mathrm{OLS}}=\left[\begin{array}{c} 2.7130\\0.7973\end{array}\right]$, thus SGD estimate is much closer compared to BGD, this can be seen when comparing Figures \ref{fig:slrbgd} and \ref{fig:slrsgd}. Also notice that the terminal estimate of SGD went back from its course in Figure \ref{fig:slrsgd} (a) as black points is evident on top of it. This is expected since again SGD is stochastic, as depicted on its loss function in Figure \ref{fig:slrsgd} (b). Zooming in to the last 500 steps of SGD shows random fluctuations of gradient vector as seen in Figure \ref{fig:sgdlookclose}.
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p11}
\caption{SGD on Loss Function Surface.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p10}
\caption{SGD on Loss Function Contour.}
\end{subfigure}
\end{minipage}
\caption[A Closer Look at SGD Gradient Vectors]{\it A Closer Look at SGD Gradient Vectors.}
\label{fig:sgdlookclose}
\end{figure}
\end{exmp}
In practice SGD is the preferred algorithm for most of the Machine Learning algorithms like the error back-propagation of the ANN (Artificial Neural Networks). To boost the speed even more, computational operations is a lot faster under vectorization, which is not applicable for SGD since it only uses single data point which is regarded as scalar. In this case, one has to consider a \textit{mini-batch stochastic gradient descent} algorithm, where instead of using single data point, one can use 2 to 100 points in each iteration, this is for the purpose of exploiting matrix computation which is a lot faster. 
\section{Laplace's Approximation}\label{sec:laplace}
The simplest approximation to the posterior distribution of the parameter is the Laplace's approximation. The idea behind this procedure is to use a Gaussian approximator, $\g(x)$, such that it is centered on the mode of the target distribution, $\p(x)$. To illustrate this, suppose
\begin{equation}
\p(x)=\frac{f(x)}{Z},\quad\mathrm{where}\;Z=\int f(x)\D x.
\end{equation}
Using basic calculus, the mode\footnote{obtained using maximum \textit{a posteriori} (MAP)} of the posterior distribution, say at $x=x_{\text{MAP}}$, is achieved by taking the derivative of the objective function with respect to the $x$-axis, such that the gradient of the function is $0$ at $x=x_{\text{MAP}}$. That is,
\begin{equation}
\frac{\D f(x)}{\D x}\bigg|_{x=x_{\text{MAP}}}=0.
\end{equation}
Note that the location of the mode is not affected by the normalizing factor $Z$. The Gaussian distribution has the property that its logarithm is a quadratic function of the variables (\cite{bishop}). Then this can be written as a Taylor expansion of the $\log f(x)$ centered on the mode $x_{\text{MAP}}$. That is,
\begin{align}
\log f(x)=&\log f(x_{\text{MAP}})+\frac{\D \log f(x)}{\D x}\bigg|_{x=x_{\text{MAP}}}(x-x_{\text{MAP}})\\
&+\frac{\D^2 \log f(x)}{\D x^2}\bigg|_{x=x_{\text{MAP}}}\frac{(x-x_{\text{MAP}})^2}{2}+\mathcal{O}(x)\\
\approx&\log f(x_{\text{MAP}})+\frac{\D^2 \log f(x)}{\D x^2}\bigg|_{x=x_{\text{MAP}}}\frac{(x-x_{\text{MAP}})^2}{2}.
\end{align}
Exponentiating both sides of the above equations becomes
\begin{equation}
f(x)\approx f(x_{\text{MAP}})\exp\left[\frac{\D^2 \log f(x)}{\D x^2}\bigg|_{x=x_{\text{MAP}}}\frac{(x-x_{\text{MAP}})^2}{2}\right].
\end{equation}
So that the normalized estimator $\g(x)$ is given by
\begin{equation}
\g(x)=\left(-\frac{1}{2\pi}\frac{\D^2 \log f(x)}{\D x^2}\bigg|_{x=x_{\text{MAP}}}\right)^{1/2}\exp\left[\frac{\D^2 \log f(x)}{\D x^2}\bigg|_{x=x_{\text{MAP}}}\frac{(x-x_{\text{MAP}})^2}{2}\right].
\end{equation}
\begin{exmp}
Suppose the posterior distribution is a chi-square of the form: $\p(x)=\frac{x^{k-1}\exp\left(-\frac{x^2}{2}\right)}{Z}$, with $k$ degrees of freedom where $Z=2^{\frac{k}{2}-1}\Gamma\left(\frac{k}{2}\right),\quad x>0$. Using Laplace, the approximation to the posterior distribution is obtained as follows:\\[.3cm]
The log-likelihood of the density function is given by
\begin{equation}
\ell(x)\triangleq\log\p(x)=(k-1)\log x-\frac{x^2}{2}+\mathrm{constant}.
\end{equation}
\begin{figure}[!t]
\includegraphics[width = \linewidth]{p14}
\caption[Laplace's Approximation on $\chi^2$-Distribution]{\it Laplace's Approximation on $\chi^2$-Distribution.}
\label{fig:laplaceapprox}
\end{figure}
The mode of this posterior is then
\begin{align}
\frac{\partial}{\partial x}\log \p(x)&=\frac{k-1}{x_{\text{MAP}}}-x_{\text{MAP}}\overset{\text{set}}{=}0\\
x_{\text{MAP}}&=\sqrt{k-1}.
\end{align}
The second partial derivative of the log-likelihood with respect to $x$ evaluated at $x_{\text{MAP}}$ is
\begin{equation}\label{eq:laplaceexapprox}
-\frac{\partial^2}{\partial x^2}\log \p(x)\bigg|_{x=x_{\text{MAP}}}=1-\frac{1-k}{x_{\text{MAP}}^2}=2.
\end{equation}
Hence the estimate of the posterior is given by a Gaussian distribution with mean $\mu=\sqrt{k-1}$ and variance $\sigma^2 = \frac{1}{2}$. Figure \ref{fig:laplaceapprox} visualizes the Gaussian distribution as an approximator to the posterior distribution.
\end{exmp}
Now consider the case where $\mathbf{x}\in\mathbb{R}^{d}$, such that $\p(\mathbf{x})=\frac{f(\mathbf{x})}{Z}$. Analogous to the univariate case, the Laplace's approximation for $\p(\mathbf{x})$ is obtained by aligning the mean of the multivariate Gaussian distribution to the mode of the posterior density, denoted by $\mathbf{x}_{\text{MAP}}$. As before, the log-likelihood of $f(\mathbf{z})$ is given by the following equation
\begin{equation}\label{eq:laplacemultivar}
\log f(\mathbf{x})\approx\log f(\mathbf{x}_{\text{MAP}})-\frac{1}{2}(\mathbf{x}-\mathbf{x}_{\text{MAP}})^{\text{T}}\boldsymbol{\mathfrak{H}}(\mathbf{x}-\mathbf{x}_{\text{MAP}}),
\end{equation}
where $\boldsymbol{\mathfrak{H}}$ is the Hessian matrix defined by
\begin{equation}\label{eq:2ndderivlaplace}
\boldsymbol{\mathfrak{H}}\triangleq-\frac{\partial^2}{\partial\mathbf{x}^2}\log f(\mathbf{x})\bigg|_{\mathbf{x}=\mathbf{x}_{\text{MAP}}}.
\end{equation}
Exponentiating Equation (\ref{eq:laplacemultivar}) leads to the normalized approximator, $\g(\mathbf{x})=\mathcal{N}_d(\mathbf{x}|\mathbf{x}_{\text{MAP}},\boldsymbol{\mathfrak{H}}^{-1})$.
\section{Markov Chain Monte Carlo (MCMC)}
Laplace has the advantage of being simple and easy to use. But like any other approximator, it has limitations especially on multimodal densities since it uses Gaussian as estimate to the posterior distribution. Unfortunately, most interesting high dimensional Bayesian models have multimodal \textit{a posteriori}. To address this problem, Bayesians resort into sampling methods for approximating the \textit{a posteriori}. For univariate densities, the popular sampling methods are \textit{rejection sampling} and \textit{importance sampling}. These sampling methods use \textit{envelop} function, that is proportional to the proposal model for drawing samples from the objective distribution. The envelop function is such that it covers the target density. For this thesis, however, the Markov Chain Monte Carlo (MCMC) sampling method is used. In particular, for \textit{Metropolis-Hasting}, \textit{Gibbs sampling} and \textit{Hamiltonian Monte Carlo} MCMC algorithms. Like in rejection and importance sampling, MCMC uses proposal distribution $\g(\cdot)$ such that the random samples proposed from this distribution are accepted using some criterion.
\subsection{Metropolis-Hasting}\label{sec:metropolishasting}
The Metropolis-Hasting algorithm is the common MCMC method for drawing samples from an intractable complex Bayesian posterior distribution. The idea is to randomly walk in the support of the target density, such that the random step is governed by the proposal distribution $\g(\cdot)$. The assumption is that the posterior distribution has no closed-form solution, but the kernel, which is the unnormalized form of the target density is easy to evaluate. This is the advantage of Metropolis-Hasting algorithm where the \textit{a posteriori} is not necessarily be normalized --- often the difficulty in simplifying the model evidence of the Bayes' rule. Let $\p(\cdot)$ be the \textit{a posteriori}, then the Metropolis-Hasting algorithm is given in Algorithm \ref{algo:mcmcmh}.
\vspace{.4cm}

\begin{algorithm}[!t]
\caption{\it Metropolis-Hasting MCMC}
\label{algo:mcmcmh}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize $\boldsymbol{\theta}_{r}\sim\g(\boldsymbol{\theta}),r=0$ and $\mathscr{S}=\{\boldsymbol{\theta}_r:\boldsymbol{\theta}_r\sim\p(\boldsymbol{\theta}), \forall r\}$\vspace{.2cm}
\For {$r\in\{1,\cdots, r_{\text{max}}\}$}\vspace{.2cm}
\State Propose: $\boldsymbol{\theta}_{new}\sim\g(\boldsymbol{\theta}_{new}|\boldsymbol{\theta}_{r-1})$\vspace{.2cm}
\State Acceptance: $\alpha(\boldsymbol{\theta}_{new}|\boldsymbol{\theta}_{r-1})\triangleq\min\left\{1,\frac{\p(\boldsymbol{\theta}_{new}|\boldsymbol{\theta}_{r-1})\g(\boldsymbol{\theta}_{r-1}|\boldsymbol{\theta}_{new})}{\p(\boldsymbol{\theta}_{r-1}|\boldsymbol{\theta}_{new})\g(\boldsymbol{\theta}_{new}|\boldsymbol{\theta}_{r-1})}\right\}$\vspace{.2cm}
\State Draw $x\sim\text{Unif}(0,1)$\vspace{.2cm}
\If {$x<\alpha(\boldsymbol{\theta}_{new}|\boldsymbol{\theta}_{r-1})$}\vspace{.2cm}
\State $\boldsymbol{\theta}_{r}\triangleq\boldsymbol{\theta}_{new}$
\vspace{.2cm}
\Else \vspace{.2cm}
\State $\boldsymbol{\theta}_{r}\triangleq\boldsymbol{\theta}_{r-1}$\vspace{.2cm}
\EndIf\vspace{.2cm}
\EndFor\\\vspace{.2cm}
\Return $\mathscr{S}$.
\end{algorithmic}
\end{algorithm}
\vspace{-.3cm}
\begin{exmp}\label{exmp:mcmcmh}
As an example of the Metropolis-Hasting MCMC sampling, consider the bivariate Gaussian distribution defined below:
\begin{equation}
f(\mathbf{x}|\boldsymbol{\mu},\boldsymbol{\Sigma})
\triangleq\frac{1}{\sqrt{(2\pi)^d|\boldsymbol{\Sigma}|}}\exp\left[-\frac{1}{2}(\mathbf{x} - \boldsymbol{\mu})^{\text{T}}\boldsymbol{\Sigma}^{-1}(\mathbf{x}-\boldsymbol{\mu})\right].
\end{equation}
And suppose it has the following parameters: $\mu\triangleq[10,-10]^{\text{T}}$ and $\Sigma\triangleq\left[\begin{array}{cc}1.5^2&\rho(1.5)(1.35)\\\rho(1.5)(1.35)&1.35^2\end{array}\right]$ where $\rho \triangleq .5$; in order to draw samples from this model, the uniform distribution is used as the proposal function with parameters min = -5 and max = 5. The plot of the random samples and its kernel density are depicted in Figure \ref{fig:mcmcmh} using 10,000 iterations.
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p26}
\caption{Random Samples.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p27}
\caption{Kernel Density Estimate.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p28}
\caption{Autocorrelation of $\theta_1$.}
\label{fig:mcmcmh:w1corr}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p29}
\caption{Autocorrelation of $\theta_2$.}
\label{fig:mcmcmh:w2corr}
\end{subfigure}
\end{minipage}
\caption[Metropolis-Hasting on Target Density after Burn-in]{\it Metropolis-Hasting on Target Density after Burn-in.}
\label{fig:mcmcmh}
\end{figure}

The random samples drawn however are not independent, this is due to the design of the Metropolis-Hasting algorithm where the distribution of the candidate model depends solely on the current sample\footnote{this is the property of a Markov Chain, hence the name MCMC.}. In fact, the points are highly correlated up to some far lags. To address this problem, some diagnostics can be done such as \textit{thinning}, where every $k$th samples are taken and the rest are discarded; or using ``burn-in" where first $K^{\star}$ samples are discarded. For example, the autocorrelation plot of the samples after the burn-in period is depicted in Figures \ref{fig:mcmcmh:w1corr} and \ref{fig:mcmcmh:w2corr}.
\iffalse
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\end{minipage}
\caption[Autocorrelation from Metropolis-Hasting Samples with Thinning]{\it Autocorrelation from Metropolis-Hasting Samples with Burn-in.}
\label{fig:corrmh}
\end{figure}
\fi
\end{exmp}
\subsection{Gibbs Sampling}
Metropolis-Hasting is by far one of the easiest MCMC algorithm for drawing samples from distributions where direct sampling is not possible. It uses proposal distribution as drivers of the random walk in the support of the target density. However, for high dimensional data, the choice of appropriate proposal function is sometimes difficult. To avoid this problem, one can use Gibbs sampler as an alternative algorithm for MCMC. The only requirement is that the joint distribution of the parameters (the \textit{a posteriori}) can be decomposed into conditional distributions of each variable conditioned on the other variables. Mathematically, suppose the multivariate distribution is given by $f(\boldsymbol{\theta}|\mathscr{D})$, where $\boldsymbol{\theta}=\{\theta_1,\cdots, \theta_K\}$, then the Gibbs sampling algorithm for MCMC is given in Algorithm \ref{algo:mcmcgibbs}.
\vspace{.4cm}

\begin{algorithm}[!t]
\caption{\it Gibbs Sampling MCMC}
\label{algo:mcmcgibbs}
\begin{algorithmic}[1]\vspace{.2cm}
\item Initialize $\boldsymbol{\theta}_{r}\sim\g(\boldsymbol{\theta}),r=0$ and $\mathscr{S}=\{\boldsymbol{\theta}_r:\boldsymbol{\theta}_r\sim\p(\boldsymbol{\theta}), \forall r\}$\vspace{.2cm}
\For {$r\in\{1,\cdots, r_{\text{max}}\}$}\vspace{.2cm}
\State $\theta_1^{(new)}\sim\g(\theta_1|\theta_2,\cdots,\theta_K)$\vspace{.2cm}
\State $\theta_2^{(new)}\sim\g(\theta_2|\theta_1^{(new)},\cdots,\theta_K)$\vspace{.2cm}
\State $\theta_3^{(new)}\sim\g(\theta_3|\theta_1^{(new)},\theta_2^{(new)},\cdots,\theta_K)$\vspace{.2cm}
\State $\vdots\qquad\qquad\qquad\vdots\qquad\qquad\qquad\vdots$\vspace{.2cm}
\State $\theta_K^{(new)}\sim\g(\theta_K|\theta_1^{(new)},\theta_2^{(new)},\cdots,\theta_{K-1}^{(new)})$\vspace{.2cm}
\State $\boldsymbol{\theta}_r\triangleq[\theta_1^{(new)},\cdots,\theta_K^{(new)}]^{\text{T}}$\vspace{.2cm}
\EndFor\\\vspace{.2cm}
\Return $\mathscr{S}$.
\end{algorithmic}
\end{algorithm}
\vspace{-.3cm}
\begin{figure}[!t]
\begin{minipage}{\linewidth}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p30}
\caption{Random Samples.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p31}
\caption{Kernel Density Estimate.}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p32}
\caption{Autocorrelation of $\theta_1$.}
\label{fig:mcmcgibbs:w0corr}
\end{subfigure}
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{p33}
\caption{Autocorrelation of $\theta_2$.}
\label{fig:mcmcgibbs:w1corr}
\end{subfigure}
\end{minipage}
\caption[Gibbs Sampling on Target Density after Burn-in]{\it Gibbs Sampling on Target Density after Burn-in.}
\label{fig:mcmcgibbs}
\end{figure}
\begin{exmp}\label{exmp:gibbs}
Using the same posterior distribution as in Example \ref{exmp:mcmcmh}, the conditional distributions of the parameters conditioned on other parameters are also Gaussian with $\mu\triangleq\mu_1 + \left(\frac{\sigma_1}{\sigma_2}\right)\rho(x - \mu_2)$ and standard deviation $\sigma\triangleq\sqrt{(1 - \rho^2)\sigma_1^2}$. So that applying the Gibbs sampler for 10,000 iterations generates random samples shown in Figure \ref{fig:mcmcgibbs}. And analogous to Metropolis-Hasting algorithm, samples obtained using Gibbs are not independent but with less magnitude on its autocorrelation compared to the former. Hence as before, the same diagnostics can be done. The autocorrelation is shown in Figures \ref{fig:mcmcgibbs:w0corr} and \ref{fig:mcmcgibbs:w1corr}, which suggest a good mixing of the random samples.
\end{exmp}
\section{Bayesian Linear Regression}
This section attempts to discuss the Bayesian approach to linear regression. To begin with, let $\mathscr{D}=\{(\mathbf{x}_1,y_1),\cdots, (\mathbf{x}_K,y_K)\}$ where $\mathbf{x}_k\in\mathbb{R}^{d}, y_k\in \mathbb{R}$ be the pairwised dataset. Suppose the response values, $y_1,\cdots,y_K$, are independent given the parameter $\mathbf{w}$, and is distributed as $y_k\sim\mathcal{N}(\mathbf{w}^{\text{T}}\mathbf{x}_k,\alpha^{-1})$ such that $\alpha^{-1}=\frac{1}{\sigma^2}$ is referred to as the \textit{precision} parameter --- useful for later derivation. So that $\alpha>0$. This assumption is valid since in practice the data in the response variable are independent. In Bayesian perspective, the weights are assumed to be random and is governed by some \textit{a priori} distribution. The choice of this distribution is subjective, but choosing arbitrary \textit{a priori} can sometimes or often result to an intractable integration, especially for interesting models. This will happen when marginalizing over the parameters, as illustrated later in the derivation of the \textit{a posteriori} model of the weights. For simplicity, a conjugate prior is used for the latent weights. Specifically, assume that $\mathbf{w}\sim\mathcal{N}(\mathbf{0},\beta^{-1}\mathbf{I})$ such that $\beta>0$ is the hyperparameter assumed for now to be known. To estimate the weights, the posterior distribution based on the Bayes' rule is used and is given by
\begin{equation}\label{eq:bayesrulepost}
	\mathbb{P}(\mathbf{w}|\mathbf{y})=\frac{\mathbb{P}(\mathbf{w})\mathbb{P}(\mathbf{y}|\mathbf{w})}{\mathbb{P}(\mathbf{y})},
\end{equation}
where $\mathbb{P}(\mathbf{w})$ is the \textit{a priori} distribution of the parameter, $\mathbb{P}(\mathbf{y}|\mathbf{w})$ is the likelihood, and $\mathbb{P}(\mathbf{y})$ is the normalizing factor. The likelihood is given by
\begin{align}
    \mathbb{P}(\mathbf{y}|\mathbf{w})&=\prod_{k=1}^{K}\frac{1}{\sqrt{2\pi\alpha^{-1}}}\exp\left[-\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}\right]\\
    &=\left(\frac{\alpha}{2\pi}\right)^{K/2}\exp\left[-\sum_{k=1}^K\frac{\alpha(y_k-\mathbf{w}^{\text{T}}\mathbf{x}_k)^2}{2}\right].\label{eq:likelihood:blreg}
\end{align}
In matrix form, this can be written as
\begin{equation}
    \mathbb{P}(\mathbf{y}|\mathbf{w})\propto\exp\left[-\frac{\alpha}{2}(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})^{\text{T}}(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})\right]
\end{equation}
where $\boldsymbol{\mathfrak{A}}=\left[(\mathbf{x}_k^{\text{T}})\right]$, i.e. $\boldsymbol{\mathfrak{A}}\in(\mathbb{R}^{K}\times\mathbb{R}^d)$. This matrix is known as the \textit{design matrix}. And since $\mathbf{w}$ has the following prior distributon
\begin{equation}\label{eq:wpriori}
    \mathbb{P}(\mathbf{w})=\frac{1}{\sqrt{(2\pi)^{d}|\beta^{-1}\mathbf{I}|}}\exp\left[-\frac{1}{2}\mathbf{w}^{\text{T}}\beta\mathbf{I}\mathbf{w}\right],
\end{equation}
then the posterior distribution has the following form:
\begin{align}
    \mathbb{P}(\mathbf{w}|\mathbf{y})&\propto\exp\left[-\frac{\alpha}{2}(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})^{\text{T}}(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})\right]\exp\left[-\frac{1}{2}\mathbf{w}^{\text{T}}\beta\mathbf{I}\mathbf{w}\right]\\
&=\exp\left\{-\frac{1}{2}\left[\alpha(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})^{\text{T}}(\mathbf{y}-\boldsymbol{\mathfrak{A}}\mathbf{w})+\mathbf{w}^{\text{T}}\beta\mathbf{I}\mathbf{w}\right]\right\}.
\end{align}
Expanding the terms in the exponent, becomes
\begin{equation}\label{eq:expterms}
    \alpha\mathbf{y}^{\text{T}}\mathbf{y}-2\alpha\mathbf{w}^{\text{T}}\boldsymbol{\mathfrak{A}}^{\text{T}}\mathbf{y}+\mathbf{w}^{\text{T}}(\alpha\boldsymbol{\mathfrak{A}}^{\text{T}}\boldsymbol{\mathfrak{A}}+\beta\mathbf{I})\mathbf{w}.
\end{equation}
Next is to complete the square of the above equation, such that it resembles the inner terms of the exponential factor of the Gaussian distribution. The quadratic form of the exponential term of a $\mathcal{N}(\mathbf{w}|\boldsymbol{\mu},\boldsymbol{\Sigma}^{-1})$ is given by
\begin{align}
    (\mathbf{w}-\boldsymbol{\mu})^{\text{T}}\boldsymbol{\Sigma}^{-1}(\mathbf{w}-\boldsymbol{\mu})&=(\mathbf{w}-\boldsymbol{\mu})^{\text{T}}(\boldsymbol{\Sigma}^{-1}\mathbf{w}-\boldsymbol{\Sigma}^{-1}\boldsymbol{\mu})\\
&=\mathbf{w}^{\text{T}}\boldsymbol{\Sigma}^{-1}\mathbf{w}-
2\mathbf{w}^{\text{T}}\boldsymbol{\Sigma}^{-1}\boldsymbol{\mu}+\boldsymbol{\mu}^{\text{T}}\boldsymbol{\Sigma}^{-1}\boldsymbol{\mu}.\label{eq:expnorm}
\end{align}
By inspection, the terms in Equation (\ref{eq:expterms}) are matched up with that in (\ref{eq:expnorm}). So that
\begin{equation}\label{eq:sigmablrgauss}
    \boldsymbol{\Sigma}^{-1}=\alpha\boldsymbol{\mathfrak{A}}^{\text{T}}\boldsymbol{\mathfrak{A}}+\beta\mathbf{I}
\end{equation}
and
\begin{align}
    \mathbf{w}^{\text{T}}\boldsymbol{\Sigma}^{-1}\boldsymbol{\mu}&=\alpha\mathbf{w}^{\text{T}}\boldsymbol{\mathfrak{A}}^{\text{T}}\mathbf{y}\\
    \boldsymbol{\Sigma}^{-1}\boldsymbol{\mu}&=\alpha\boldsymbol{\mathfrak{A}}^{\text{T}}\mathbf{y}\\
    \boldsymbol{\mu}&=\alpha\boldsymbol{\Sigma}\boldsymbol{\mathfrak{A}}^{\text{T}}\mathbf{y}.\label{eq:mublrgauss}
\end{align}
Thus the posterior distribution is a Gaussian distribution with location parameter in Equation (\ref{eq:mublrgauss}) and scale parameter in (\ref{eq:sigmablrgauss}).
\begin{figure}[hbtp]
\begin{tabular}{p{.3\textwidth}p{.3\textwidth}p{.3\textwidth}}
\multicolumn{1}{c}{Likelihood}&\multicolumn{1}{c}{Prior/Posterior}&\multicolumn{1}{c}{Data Space}\\
&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p15}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p16}
%\caption{Fitted Line.}
\end{subfigure}\\
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p17}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p18}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p19}
%\caption{Fitted Line.}
\end{subfigure}\\
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p20}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p21}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p22}
%\caption{Fitted Line.}
\end{subfigure}\\
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p23}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p24}
%\caption{Prior of $\mathbf{w}$.}
\end{subfigure}&
\begin{subfigure}[b]{.3\textwidth}
\includegraphics[width=\textwidth]{p25}
%\caption{Fitted Line.}
\end{subfigure}
\end{tabular}
\caption[Sequential Bayesian Learning of Fitting a Straight Line]{\it Sequential Bayesian Learning of Fitting a Straight Line.}
\label{fig:seqbayeslearnexmp}
\end{figure}
\begin{exmp}[\it Sequential Bayesian Learning by Simulation]\label{exmp:sblbs}
To understand the application of Bayesian inference to linear regression having \textit{a priori}, likelihood, and \textit{a posteriori} presented above, this example based from (\cite{bishop}) illustrates the sequential Bayesian learning to fitting a straight line on a simulated data. Consider the input variable $x$ and a single target variable $y$ such that the true function is the simple linear regression with parameters $w_0=-.3$ and $w_1=-.5$. Suppose the random values of $x$ are taken from a uniform distribution having domain $[-1,1]$; then the target variable $y=h(x,\mathbf{w})+\varepsilon=w_0+w_1x+\varepsilon$, where $\varepsilon$ is a Gaussian noise having mean $0$ and standard deviation $5$. The goal is to recover the true value of $w_0$ and $w_1$ from such data. Using the \textit{a priori} in Equation (\ref{eq:wpriori}) for the parameters, the plot of this density is given in the first row, second column of Figure \ref{fig:seqbayeslearnexmp}. This is the case in which there is no data point yet observed. The white diamond point in the contour plot of the prior density is the true value of the parameters that needs to be estimated. The corresponding 20 samples of straight lines with weights sampled from the \textit{a priori} are plotted in the far right hand side of the first row of the said figure. The second row depicts the case where first observation is observed. The likelihood of this data point is shown in the left hand side of the said row. Using this likelihood multiplied with the \textit{a priori} in the previous row, and further multiply it with the normalizing constant, gives the \textit{a posteriori} in the middle column of the second row. The corresponding fitted line is placed in the right hand side of the said row. Following the same procedure for the third row, with likelihood of the fifth observation shown in the left hand side of the third row, and \textit{a priori} given by the \textit{a posteriori} of the second row, returns a more concentrated posterior distribution of the weights with 20 sampled fitted line in the third column of the said row. And so on until the fourth column consisting 20 observations.
\iffalse
\begin{figure}[!t]
\begin{minipage}{\textwidth}
\begin{subfigure}[b]{.333333\textwidth}
\includegraphics[width=\textwidth]{p15}
\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.333333\textwidth}
\includegraphics[width=\textwidth]{p15}
\caption{Prior of $\mathbf{w}$.}
\end{subfigure}
\begin{subfigure}[b]{.333333\textwidth}
\includegraphics[width=\textwidth]{p16}
\caption{Fitted Line.}
\end{subfigure}
\end{minipage}
\caption[Prior Distribution  of $\mathbf{w}$]{\it Prior Distribution  of $\mathbf{w}$.}
\label{fig:seqbayeslearnexmp}
\end{figure}
\fi
\end{exmp}
\section{Bayesian Logistic Regression}


}
